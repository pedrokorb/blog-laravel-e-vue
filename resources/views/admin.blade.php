@extends('layouts.app')

@section('content')
<pagina-component tamanho="10">

    <painel-component titulo= "Dashboard">
        <migalhas-component v-bind:lista="{{$listaMigalhas}}"></migalhas-component>
        <div class="row">

            <div class="col-md-4">

                <caixa-component
                        qtd="{{$totalPontos}}"
                        titulo="Pontos"
                        url="{{route('pontos.index')}}"
                        cor="green"
                        icone="ion-home">
                </caixa-component>
            </div>

            @can('autor')
                <div class="col-md-4">
                
                    <caixa-component 
                    qtd="{{$totalArtigos}}"
                    titulo="Artigos"
                    url="{{route('artigos.index')}}"
                    cor="orange"
                    icone="ion ion-pie-graph">
                    </caixa-component>
                </div>
            @endcan
                
            @can('eAdmin')
                
                <div class="col-md-4">
                    
                    <caixa-component 
                    qtd="{{$totalUsuarios}}"
                    titulo="Usuários"
                    url="{{route('usuarios.index')}}"
                    cor="blue"
                    icone="ion ion-person-stalker">
                    </caixa-component>
    
                </div>
                
                <div class="col-md-4">
                    
                    <caixa-component 
                    qtd="{{$totalAutores}}"
                    titulo="Autores"
                    url="{{route('autores.index')}}"
                    cor="red"
                    icone="ion ion-person">
                    </caixa-component>
    
                </div>
    
                <div class="col-md-4">
                    
                    <caixa-component 
                    qtd="{{$totalAdmin}}"
                    titulo="Admins"
                    url="{{route('adm.index')}}"
                    cor="green"
                    icone="ion ion-person">
                    </caixa-component>
    
                </div>
            @endcan
                
        </div>

    </painel-component>

</pagina-component>
@endsection
