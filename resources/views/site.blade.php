@extends('layouts.app')

@section('content')
    <pagina-component tamanho="12">
        <painel-component titulo="Artigos">

            <a href="admin/pontos">Bater Ponto</a> <br>
            <a href="multiple-file-upload">[TESTE] Upload de múltiplas imagens</a>
            <div class="row h-100 justify-content-center align-items-center">
                <p>
                    <form class="form-inline text-center" action="{{route('site')}}" method="get" >
                        <input type="search" class="form-control" name="busca" placeholder="Buscar" value="{{isset($busca) ? $busca : ""}}">
                        <button class="btn btn-info">Buscar</button>
                    </form>
                </p>
                
            </div>
            <p></p>

            <qr-stream> </qr-stream>

            <div class="card-deck">

                @foreach ($lista as $key => $value)
                    <card-artigo-component
                    titulo="{{str_limit($value->titulo, 30, "...")}}"
                    descricao="{{str_limit($value->descricao, 30, "...")}}"
                    link="{{route('artigo', [$value->id, str_slug($value->titulo)])}}"
                    imagem="https://as2.ftcdn.net/jpg/02/23/40/63/500_F_223406364_uyK6rtziI4CPAV7tfsqiPcVuKj9La7zL.jpg"
                    data="{{$value->data}}"
                    autor="{{$value->autor}}"
                    sm="6"
                    md="4"
                    >
                    </card-artigo-component>
                @endforeach
                
            </div>
            <div class="row h-100 justify-content-center align-items-center">
                    {{$lista->links()}}
            </div>


            
        </painel-component>
    </pagina-component>
@endsection

<script>
    import QrDetect from "../js/components/QrDetect";
    export default {
        components: {QrDetect}
    }
</script>
<script>
    import QrDetect from "../js/components/QrDetect";
    export default {
        components: {QrDetect}
    }
</script>
<script>
    import QrDetect from "../js/components/QrDetect";
    export default {
        components: {QrDetect}
    }
</script>
<script>
    import QrStream from "../js/components/QrStream";
    export default {
        components: {QrStream}
    }
</script>