@extends('layouts.app')

@section('content')
<pagina-component tamanho="10" xmlns:v-bind="http://www.w3.org/1999/xhtml">

    @if($errors->all())
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
      @foreach ($errors->all() as $key =>  $value)
        <li><strong>{{$value}}</strong></li>
      @endforeach
    </div>
    @endif

    <painel-component titulo= "Lista de Pontos">

        <migalhas-component v-bind:lista="{{$listaMigalhas}}"></migalhas-component>


        <tabela-lista-component 
        v-bind:titulos="['#', 'Tipo', 'Data', 'Horario', 'Usuario']"
        v-bind:itens="{{json_encode($listaPontos)}}"
        ordem="asc" ordemcol="0"
        ponto="#criar"
        modal="sim">
        </tabela-lista-component>

        <div class="row h-100 justify-content-center align-items-center">
            {{$listaPontos->links()}}

        </div>
    </painel-component>

</pagina-component>

<modal-component nome="adicionar" titulo="Adicionar">
<formulario-component id="formAdicionar" css="" action="{{route('pontos.store')}}" method="post" enctype="" token="{{ csrf_token() }}">

    <div class="form-group">
        <label for="tipo">Tipo</label>
        <select class="form-control" name="tipo" id="tipo">
            <option {{(old('tipo') && old('tipo') == 'Entrada' ? 'selected' : '' )}} value="Entrada">Entrada</option>
            <option {{(old('tipo') && old('tipo') == 'Entrada' ? 'selected' : '' )}} {{(!old('tipo') ? 'selected' : '')}} value="Saída">Saída</option>
        </select>
    </div>

    <p>Para completar o seu ponto, leia seu código QR</p>
    <camera-qr-component></camera-qr-component>


    <div class="form-group" hidden="true">
        <label for="idUser">IdUser</label>
        <input type="text" class="form-control" id="idUser" name="idUser" placeholder="IdUser" v-model="$store.state.resultado">
    </div>

    
  </formulario-component>
  <span slot="botoes">
    <button form="formAdicionar" class="btn btn-info">Adicionar</button>
  </span>
</modal-component>

@endsection
<script>
    import Pagina from "../../../js/components/Pagina";
    export default {
        components: {Pagina}
    }
</script>