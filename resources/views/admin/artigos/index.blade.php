@extends('layouts.app')

@section('content')
<pagina-component tamanho="10">

    @if($errors->all())
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
      @foreach ($errors->all() as $key =>  $value)
        <li><strong>{{$value}}</strong></li>
      @endforeach
    </div>
    @endif

    <painel-component titulo= "Lista de Artigos">

        <migalhas-component v-bind:lista="{{$listaMigalhas}}"></migalhas-component>


        <tabela-lista-component 
        v-bind:titulos="['#','Título','Descrição','Autor', 'data']"
        v-bind:itens="{{json_encode($listaArtigos)}}"
        ordem="desc" ordemcol="0"
        criar="#criar" detalhe="/admin/artigos/" editar="/admin/artigos/" deletar="/admin/artigos/" token="{{ csrf_token() }}"
        modal="sim">
        </tabela-lista-component>

        <div class="row h-100 justify-content-center align-items-center">
          {{$listaArtigos->links()}}
        </div>
    </painel-component>

</pagina-component>

<modal-component nome="adicionar" titulo="Adicionar">
<formulario-component id="formAdicionar" css="" action="{{route('artigos.store')}}" method="post" enctype="" token="{{ csrf_token() }}">
    
      <div class="form-group">
        <label for="titulo">Título</label>
        <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Título" value="{{old('titulo')}}">
      </div>
      <div class="form-group">
        <label for="descricao">Descrição</label>
        <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Descrição" value="{{old('descricao')}}">
      </div>

      <div class="form-group">
        <label for="addConteudo">Conteúdo</label>
        <ckeditor-component
          id="addConteudo"
          name="conteudo"
          value="{{old('conteudo')}}" 
          v-bind:config="{
            toolbar: [
              [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript' ]
            ],
            height: 200}" 
          >
        </ckeditor-component>

      </div>

      <div class="form-group">
        <label for="data">Data</label>
        <input type="date" class="form-control" id="data" name="data" value="{{old('data')}}">
      </div>
    
  </formulario-component>
  <span slot="botoes">
    <button form="formAdicionar" class="btn btn-info">Adicionar</button>
  </span>
</modal-component>

<modal-component nome="editar" titulo="Editar">
  <formulario-component id="formEditar" v-bind:action="'/admin/artigos/' + $store.state.item.id" method="put" enctype="" token="{{ csrf_token() }}">
    
    <div class="form-group">
      <label for="titulo">Título</label>
      <input type="text" class="form-control" id="titulo" name="titulo" v-model="$store.state.item.titulo" placeholder="Título">
    </div>
    <div class="form-group">
      <label for="descricao">Descrição</label>
      <input type="text" class="form-control" id="descricao" name="descricao" v-model="$store.state.item.descricao" placeholder="Descrição">
    </div>
    <div class="form-group">
      <label for="editConteudo">Conteúdo</label>
      
      <ckeditor-component
          id="editConteudo"
          name="conteudo"
          v-model="$store.state.item.conteudo" 
          v-bind:config="{
            toolbar: [
              [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript' ]
            ],
            height: 200}" 
          >
      </ckeditor-component>
    </div>

    <div class="form-group">
      <label for="data">Data</label>
      <input type="date" class="form-control" id="data" name="data" v-model="$store.state.item.data">
    </div>
  </formulario-component>
  <span slot="botoes">
    <button form="formEditar" class="btn btn-info">Salvar</button>
  </span>
</modal-component>

<modal-component nome="detalhe" v-bind:titulo="$store.state.item.titulo">
  <p>@{{$store.state.item.descricao}}</p>
  <p>@{{$store.state.item.conteudo}}</p>
</modal-component>
@endsection
