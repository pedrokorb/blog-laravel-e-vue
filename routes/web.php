<?php
use App\Artigo;
use \App\Http\Controllers\Admin\ArtigosController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Request $req) {

    if(isset($req->busca) && $req->busca != ""){
        $busca = $req->busca;
        $lista = ArtigosController::listaArtigosSite(3,$busca);

      // busca
    }else{
        $lista = ArtigosController::listaArtigosSite(3);
        $busca = "";
    }

    return view('site',compact('lista','busca'));
})->name('site');


Route::get('/artigo/{id}/{titulo?}', function ($id) {
    $artigo = Artigo::find($id);
    if($artigo){
        return view('artigo', compact('artigo'));
    }
    return redirect()->route('site');
    
})->name('artigo');

Route::get('/admin/qr-code/{id}', function ($id) {
    return QrCode::size(500)->generate($id);
})->name('qr');

Route::get('/uploadfile', 'UploadfileController@index');
Route::post('/uploadfile', 'UploadfileController@upload');

Route::get('multiple-file-upload', 'MultipleUploadController@index');

Route::post('multiple-file-upload/upload', 'MultipleUploadController@upload')->name('upload');

Auth::routes();

Route::get('/admin', 'AdminController@index')->name('admin');

Route::middleware(['auth'])->prefix('admin')->namespace('Admin')->group(function(){
    Route::resource('artigos', 'ArtigosController')->middleware('can:autor');
    Route::resource('pontos', 'PontosController');
    Route::resource('usuarios', 'UsuariosController')->middleware('can:eAdmin');
    Route::resource('autores', 'AutoresController')->middleware('can:eAdmin');
    Route::resource('adm', 'AdminController')->middleware('can:eAdmin');
});
