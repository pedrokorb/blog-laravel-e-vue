<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artigo;
use App\Ponto;
use App\User;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $listaMigalhas = json_encode([
            ["titulo"=>"Admin","url"=>""]
        ]);

        $totalUsuarios = User::count();
        $totalArtigos = Artigo::count();
        $totalPontos = Ponto::count();
        $totalAutores = User::where('autor', '=', 'S')->count();
        $totalAdmin = User::where('admin', '=', 'S')->count();


        return view('admin', compact('listaMigalhas', 'totalUsuarios', 'totalPontos', 'totalArtigos', 'totalAutores', 'totalAdmin'));
    }
}
