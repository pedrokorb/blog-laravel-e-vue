<?php

namespace App\Http\Controllers\Admin;

use App\Ponto;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class PontosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listaMigalhas = json_encode([
            ["titulo"=>"Admin","url"=>route('admin')],
            ["titulo"=>"Lista de Pontos","url"=>""]
        ]);

        $listaPontos = PontosController::listaPontos(5);

        return view('admin.pontos.index',compact('listaMigalhas','listaPontos'));
    }

    public static function listaPontos($paginate){
        /*
               $listaPontos = Ponto::select('id','tipo','horario','user_id')->paginate($paginate);

               foreach($listaPontos as $key => $value){
                   $value->user_id = User::find($value->user_id)->name;
               }

              */

       $user = auth()->user();

       if($user->admin == "S"){
           $listaPontos = DB::table('pontos')
               ->join('users', 'users.id', '=', 'pontos.user_id')
               ->select('pontos.id','pontos.tipo', 'pontos.data','pontos.horario', 'users.name')
               ->whereNull('deleted_at')
               ->paginate($paginate);
       } else {
           $listaPontos = DB::table('pontos')
               ->join('users', 'users.id', '=', 'pontos.user_id')
               ->select('pontos.id','pontos.tipo', 'pontos.data','pontos.horario','users.name')
               ->whereNull('deleted_at')
               ->where('pontos.user_id', '=', $user->id)
               ->paginate($paginate);

       }

        return $listaPontos;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $data['horario'] = Carbon::now()->toTimeString();
        $data['data'] = Carbon::now()->toDateString();

        $user = auth()->user();
        if($user['id'] == $data['idUser']){
            $user->pontos()->create($data);
            return redirect()->back();
        } else{
            dd("Erro! ID do QR Code é diferente do ID do usuario");
            return redirect()->back();
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
