**Projeto Blog Jetimob com controle de Ponto QrCode**
-
Listagem de artigos com um campo de busca para filtrar artigos por título e descrição.
Controle de ponto com QrCode. Cada usuário possui seu próprio QrCode, que ao bater o ponto, deve ser escaneado.
Apenas o administrador pode cadastrar novos usuários na plataforma.

Área de administração todos podem acessar, porém, cada usuário com sua permissão poderá visualizar alguma parte do sistema:
- **Usuário:** Apenas pode ver e bater seu ponto;
- **Autor:** Pode ver e bater seu ponto, pode criar e ver seus artigos;
- **Administrador:** Tem controle total sobre o sistema. Pode ver os pontos de todos, pode ver os artigos de todos, pode cadastrar outros administradores, autores e usuários.

Projeto em Laravel 5.8, utilizando banco de dados SqLite e Vue.js

Curso na Udemy:
https://www.udemy.com/laravel-55-com-vue-js

Laravel:
https://laravel.com/docs/5.8

**Após clonar esse repositório:**
- Rodar o comando `composer install`
- Duplicar arquivo .env.example e renomeá-lo apenas como .env
- Você necessitará de uma API key. Para obtê-la, deverá rodar o comando `php artisan key:generate`
- Configurar o banco de dados: no arquivo .env, localizar a linha onde está setado o banco de dados a ser utilizado e atualizá-la para `DB_CONNECTION=sqlite`
- Criar o arquivo `database.sqlite` dentro do diretório database
- `npm install` para instalar todas as dependências
- Rodar o servidor PHP com o comando `php artisan serve`



